package ru.tsc.chertkova.tm.api.service;

import ru.tsc.chertkova.tm.model.Command;

public interface ICommandService {

    Command[] getCommands();

}
